<?php namespace MyApp\Models {

use EasilyPHP\Database\SqlMySQL;

class colegio
{
    private $db = null;

    public function __construct($config)
    {
      $this->db = new SqlMySQL($config['server'], $config['database'], $config['user'], $config['password']);
    }

    /**
     * Obtiene todos los registros de usuario
     */
    public function getAllColegio()
    {
      $this->db->connect();
      $result = $this->db->runSql("SELECT * FROM colegio");
      $this->db->disconnect();
      return $this->db->getAll($result);
    }

    /**
     * Obtiene un registro de usuario
     */
    public function getColegio($id)
    {
      $this->db->connect();
      $result = $this->db->runSql("SELECT * FROM colegio WHERE id=".$id);
      $this->db->disconnect();
      return $this->db->nextResultRow($result);
    }

    /**
     * Inserta un nuevo registro de usuario en base de datos
     * @param user contiene los datos del nuevo registro de usuario
     */
    public function insertColegio($colegio)
    {
     

      $this->db->connect();
      $sql = "INSERT INTO colegio(nombre,direccion,region,cantidadEstud,fecha,tipoColegio) VALUES (?, ?, ?, ?, ?,?)";

      if($stmt = $this->db->prepareSQL($sql)) {
        $stmt->bind_param("ssssss", $colegio['nombre'], $colegio['direccion'], $colegio['region'], $colegio['cantidadEstud'], $colegio['fecha'],$colegio['tipoColegio']);
        $stmt->execute();
        $stmt->close();
      } else {
          echo $this->db->getError();
          exit;
      }
      $this->db->disconnect();
    }
  

    public function updateColegio($request)
    {
      $this->db->connect();
      
        $sql = "UPDATE colegio SET nombre = ?, direccion = ?, region = ?, cantidadEstud = ?, fecha = ?, tipoColegio = ? WHERE id = ?";

      $stmt = $this->db->prepareSQL($sql);
      if($stmt) {
      
          $stmt->bind_param("sssssss", $request['nombre'], $request['direccion'], $request['region'], $request['cantidadEstud'], $request['fecha'],$request['tipoColegio'],$request['id']);
        $stmt->execute();
        $stmt->close();
      } else {
          echo $this->db->getError();
          
      }
      $this->db->disconnect();
    }

    /**
    * Elimina un registro de la base de datos
    * @param id del registro que se desea eliminar
    */
    public function deleteColegio($id)
    {
      $this->db->connect();
      $sql = "DELETE FROM colegio WHERE id = ?";

      if($stmt = $this->db->prepareSQL($sql)) {
        $stmt->bind_param("i", $id);
        $stmt->execute();
        $stmt->close();
      } else {
          echo $this->db->getError();
          exit;
      }
      $this->db->disconnect();
    }

  
   
   
}
}