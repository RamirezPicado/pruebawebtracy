<?php namespace MyApp\Controllers {

use MyApp\Models\colegio;
use MyApp\Utils\Message;

class ColegiosController
{
    private $config = null;
    private $login = null;
    private $colegiosModel = null;

    /**
     * Constructor de controlador de usuarios
     * 
     * @param config contiene la configuración para la conexión de BD
     * @param login contiene los datos del usuario que inició sesión
     */
    public function __construct($config, $login)
    {
        $this->config = $config;
        $this->login = $login;
    }


    public function index(){
        $colegiosModel = new colegio($this->config);
        $collection = $colegiosModel->getAllColegio();

        $dataToView = ["collection" => $collection,
                       "login" => $this->login ];

        return view("colegio/index.php", $dataToView);
    }

    /**
     * Muestra el formulario para crear un nuevo registro
     */
    public function create()
    {
        $dataToView = [ "login" => $this->login ];
        return view("colegio/new.php", $dataToView);
    }
    /**
     * Almacena un registro nuevo de "user" en la base de datos
     * @param request contiene los datos del nuevo registro de usuario
     */
    public function store($request)
    {
        $colegiosModel = new colegio($this->config);
        $colegiosModel->insertColegio($request);
        header('Location: /colegio');
    }

    /**
     * Muestra la vista "show" con los detalles del registro "id"
     * @param id del registro del cual se deben mostrar los detalles
     */
    public function show($id){
        $colegiosModel = new colegio($this->config);
        $item = $colegiosModel->getColegio($id);
        $dataToView = ["login" => $this->login,
                       "item" => $item ];

        // En cualquier otro caso se renderiza la vista 'show'
        return view("colegio/show.php", $dataToView);
    }

    /**
    * Muestra para vista "edit" con los detalles del registro "id"
    * @param id del registro del cual se deben cargar los detalles para edición
    */
    public function edit($id){
        $colegiosModel = new colegio($this->config);
        $item = $colegiosModel->getColegio($id);
        $dataToView = ["login" => $this->login,
                       "item" => $item ];
        return view("colegio/edit.php", $dataToView);
    }

    /**
    * Actualiza un registro de "user" en la base de datos
    * @param request contiene los datos del registro de usuario a actualizar
    */
    public function update($request)
    {
        $colegiosModel = new colegio($this->config);
        $colegiosModel->updateColegio($request);
        //$this->index();
        header('Location: /colegio');
    }

    /**
     * Elimina un registro de la base de datos
     * @param id del registro que se desea eliminar
     */
    public function destroy($id)
    {
        $colegiosModel = new colegio($this->config);
        $colegiosModel->deleteColegio($id);
        header('Location: /colegio');
    }

    
}

   
    }