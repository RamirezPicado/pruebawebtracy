<?php 
  include VIEWS.'/partials/header.php';

?>
  <div class="container"><br>
    <div class="row">
      <div class="col-sm-12">
        <h1>Colegio</h1>
        <table class="table table-striped">
          <thead>
            <tr>
              <th class="text-center">Ver</th>
              <th class="text-center">Editar</th>
              <th class="text-center">Eliminar</th>
              <th scope="col">Id</th>
              <th scope="col">Nombre de Colegio</th>
              <th scope="col">Dirección</th>
              <th scope="col">Región</th>
              <th scope="col">Cantidad de Estudiantes</th>
              <th scope="col">Fecha</th>
              <th scope="col">Tipo de Colegio</th>
            </tr>
          </thead>
          <tbody>
            
            <?php foreach ($collection as $item): ?>
            <tr>
              <td class="text-center">
                <a class="btn btn-sm btn-secondary fas fa-eye" href="<?= "/colegio/index.php?show=".$item['id']; ?>"></a>
              </td>
              <td class="text-center">
                <a class="btn btn-sm btn-secondary fas fa-edit" href="<?= "/colegio/index.php?edit=".$item['id']; ?>"></a>
              </td>
              <td class="text-center">
                <a class="btn btn-sm btn-secondary fas fa-trash" href="<?= "/colegio/index.php?delete=".$item['id']; ?>"></a>
              </td>
              <td><?= $item['id']; ?></td>
              <td><?= $item['nombre']; ?></td>
              <td><?= $item['direccion']; ?></td>
              <td><?= $item['region']; ?></td>
              <td><?= $item['cantidadEstud']; ?></td>
              <td><?= $item['fecha']; ?></td>

              <td><?= $item['tipoColegio']=='L' ? 'Liceo' : 'Tecnico' ?></td>
            </tr>
            <?php endforeach; ?>
          </tbody>
        </table>
        <a class="btn btn-primary" href="/colegio/index.php?action=new">Agregar Colegio</a>
      </div>
    </div>
  </div>
  <?php include VIEWS.'/partials/footer.php' ?>
