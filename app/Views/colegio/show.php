<?php include VIEWS.'/partials/header.php';
      ?>
  <div class="container">
    <br>
    <div class="row">
      <div class="col-sm-6">
        <h1>Editar Colegios</h1>
        <form action="/colegio/index.php?action=update" method="post">
         
          <input type="hidden" name="id" value="<?php echo $item["id"]; ?>" readonly>
          <div class="form-group">
            <label for="nombre">Nombre</label>
            <input 
              type="text" class="form-control" id="nombre" name="nombre"
              value="<?php echo $item["nombre"];   ?> "  readonly>
          </div>
          <div class="form-group">
            <label for="direccion ">Dirección</label>
            <input 
              type="text" class="form-control" id="direccion " name="direccion "
              value="<?php echo $item["direccion"]; ?>" readonly>
          </div>
          <div class="form-group">
            <label for="region">Region</label>
            <input 
              type="region" class="form-control" id="region" name=" region"
              value="<?php echo $item["region"]; ?>" readonly>
         
          </div>
        
                
          <div class="form-group">
            <label for="cantidadEstud">Cantidad Estudiantes</label>
            <input 
              type="cantidadEstud" class="form-control" id="cantidadEstud" name="cantidadEstud"
              value="<?php echo $item["cantidadEstud"]; ?>" readonly>
          </div>
       
          <div class="form-group">
            <label for="fecha">Fecha</label>
            <input 
              type="fecha" class="form-control" id="fecha" name="fecha"
              value="<?php echo $item["fecha"]; ?>"  readonly>

            <div class="form-group">
            <label for="tipoColegio">Tipo de Colegios</label>
            <select class="form-control" id="tipoColegio" name="tipoColegio">
              <option value="L" <?php echo $item["tipoColegio"] == 'L' ? 'selected' : '' ?> readonly>Liceo</option>
              <option value="T" <?php echo $item["tipoColegio"] == 'T' ? 'selected' : '' ?>readonly>Tecnico</option>
            </select>


          </div>
          <button type="submit" class="btn btn-primary">Guardar</button>
          <a class="btn btn-secondary" href="/colegio/index.php">Regresar</a>
        </form>
      </div>
    </div>
  </div>
  <?php 
   
    include VIEWS.'/partials/footer.php' ?>
 