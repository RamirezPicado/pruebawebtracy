<?php include VIEWS.'/partials/header.php';
      ?>
  <div class="container">
    <br>
    <?php include VIEWS.'/partials/message.php' ?>
    <div class="row">
      <div class="col-sm-6">
        <h1>Agregar Colegios</h1>
        <form action="/colegio/index.php?action=save" method="post">
          <div class="form-group">
            <label for="nombre">Nombre colegio</label>
            <input 
              type="text" class="form-control" id="nombre" name="nombre">
          </div>
          <div class="form-group">
            <label for="direccion">Direccion</label>
            <input 
              type="text" class="form-control" id="direccion" name="direccion">
          </div>
          <div class="form-group">
            <label for="region">Región</label>
            <input 
              type="text" class="form-control" id="region" name="region">
          </div>

          <div class="form-group">
            <label for=" cantidadEstud">Cantidad Estudiantes</label>
            <input 
              type="text" class="form-control" id="cantidadEstud" name="cantidadEstud">
          </div>

          <div class="form-group">
            <label for="fecha">Fecha</label>
            <input 
              type="text" class="form-control" id="fecha" name="fecha">
          </div>

          <div class="form-group">
            <label for="tipoColegio">Tipo de Colegio</label>
            <select class="form-control" id="tipoColegio" name="tipoColegio">
              <option value="L">Liceo</option>
              <option value="T">Tecnico</option>
            </select>
          </div>

      

          <button type="submit" class="btn btn-primary">Guardar</button>
          <a class="btn btn-secondary" href="/colegio/index.php">Regresar</a>
        </form>
      </div>
    </div>
  </div>
  <?php include VIEWS.'/partials/footer.php' ?>