<footer>
    <script src="/assets/js/jquery-3.3.1.min.js" charset="utf-8"></script>
    <script src="/assets/js/bootstrap.bundle.min.js" charset="utf-8"></script>
    <script src="/assets/js/main.js" charset="utf-8"></script>
<?php if (isset($scriptJS)) : ?>
    <script src="/assets/js/<?= $scriptJS ?>.js" charset="utf-8"></script>
<?php endif; ?>
  </footer>
</body>
