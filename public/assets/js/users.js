// La siguiente línea es equivalente a: $(document).ready(function()
// solo que esta es nativa de JavaScript, mientras que la otra
// es de jQuery
document.addEventListener('DOMContentLoaded', function() { 
    console.log('Se ha cargado el script de usuarios');
    
    // Se captura el input hidden 'view' para determinar
    // cuál es la vista que está mostrándose y así
    // cargar el bloque de código que corresponda
    let view = document.getElementById('view').value;

    // Bloque de código para la vista 'Edit'
    if (view == "edit")
    {
        console.log('Se ha cargado el bloque JS para la vista Edit');
        let inputPassword = document.getElementById('password');
        let showPassword = document.getElementById('showPassword');
        showPassword.addEventListener('click', function() {
            inputPassword.getAttribute("")

            if (showPassword.checked) {
                inputPassword.type="text"
            } else {
                inputPassword.type="password"
            }
        });
    }

    // Bloque de código para la vista 'Show'
    if (view == "show")
    {
        console.log('Se ha cargado el bloque JS para la vista Show');
        let currentID = parseInt($('#id').val());
        let reachedEnd = false; // Se activa cuando se llega al inicio o al final
        let lastDirection = null;

        // Método para mostrar el siguiente registro
        $("#previousRecord").click(function() {
            if (!reachedEnd || lastDirection != 'previous') {
                currentID--;
                lastDirection = 'previous';
            }
            showOnJSON(); // Llamada al método AJAX
            console.log("Previous: " + currentID);
        });

        // Método para mostrar el anterior registro
        $("#nextRecord").click(function() {
            if (!reachedEnd || lastDirection != 'next') {
                currentID++;
                lastDirection = 'next';
            }
            showOnJSON(); // Llamada al método AJAX
            console.log("Next: " + currentID);
        });

        function showOnJSON()
        {
            /**
             * ------------------------------------------------------
             * Llamada por AJAX al método 'show' del UsersController
             * ------------------------------------------------------
             * Este request es canalizado por el 'Dispatcher', el cual
             * al detectar el parámetro GET 'format'='json' invoca al
             * método 'show', el cual responde en formato JSON
             * 
             */
            $.ajax({
                url: '/users/index.php',
                type: 'GET',
                data: { show: currentID, format: 'json' },
                success: function (response){
                    if (response != null) {
                        console.log(response);
                        currentID = parseInt(response['id']);
                        reachedEnd = false;
                        updateShowView(response);
                    }
                    else {
                        reachedEnd = true;
                        /**
                         * Como se alcanzó el final hay que devolver el 'head',
                         * que es el puntero con el cual se hace el desplazamiento,
                         * en este algoritmo se hace con la variable 'currentID'
                         */
                        if (lastDirection == 'next')
                            currentID--;
                        else
                            currentID++;
                    }
                        
                }
            });

        }

        /**
         * Esta función se encarga de actualizar los 'inputs' de la
         * vista 'show', incluyendo el atributo 'href' del 'anchor'
         * que llama al método 'edit' para cargar la vista de edición
         * 
         * @param {contiene los datos de un registro de usuario} user 
         */
        function updateShowView(user)
        {
            // La actualización se hace con selectores de jQuery
            $('#id').val(user['id']);
            $('#fullname').val(user['fullname']);
            $('#username').val(user['username']);
            $('#role').val(user['role'] == 'S' ? 'Superusuario' : 'Regular');
            $('#blocked').val(user['blocked'] == 'Y' ? 'Bloqueado' : 'Desbloqueado');
            $('#editAnchor').attr('href','/users/index.php?edit=' + user['id']);
        }
    }
});